//
// Created by Midi12 on 28/05/16.
//

#include "../xmem/XMem.hpp"

#include <iostream>
#include <vector>

using namespace XMem;

int main(int argc, char* argv[])
{
    std::cout << "cheching for privileges ... " << std::endl;

    if (!Helpers::check_for_elevated_privileges())
    {
        std::cout << "necessary privileges not held, retry with root privileges" << std::endl;
        return 0;
    }

    std::vector<Process> processes = Process::EnumerateProcesses();

    for (Process& p : processes)
    {
        std::vector<Thread> threads = Thread::EnumerateThreads(p.GetPid());

        std::cout << "process : " << p.GetName() << " (" << p.GetPid() << ") has " << threads.size() << " threads" << std::endl;

        threads.clear();
    }

    return 0;
}