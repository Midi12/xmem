//
// Created by Midi12 on 28/05/16.
//

#include "../xmem/XMem.hpp"

#include <iostream>
#include <vector>
#include <regex>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/wait.h>

using namespace XMem;

void fn(void)
{
    std::cout << "HELLO" << std::endl;
}

int main(int argc, char* argv[])
{
    if (!Helpers::check_for_elevated_privileges())
    {
        std::cout << "necessary privileges not held, retry with root privileges" << std::endl;
        return 0;
    }

    std::vector<Process> processes = Process::EnumerateProcesses("test_xmem_signature");

    if (processes.empty())
    {
        std::cout << "not found ?" << std::endl;
        return 0;
    }

    processes.front().Attach();

    if (!processes.front().IsAttached())
    {
        std::cout << "cannot attach ?" << std::endl;
        return 0;
    }

    std::ifstream maps_file("/proc/" + std::to_string(processes.front().GetPid()) + "/maps");

    if (!maps_file.is_open())
    {
        std::cout << "cannot open maps file ?" << std::endl;
        return 0;
    }

    std::ifstream mem_file("/proc/" + std::to_string(processes.front().GetPid()) + "/mem", std::ios::binary);

    if (!mem_file.is_open())
    {
        std::cout << "cannot open mem file ?" << std::endl;
        return 0;
    }

    // regex to gather necessary data to dump mem
    //const std::regex expr("([0-9a-fA-F]{8,16})-([0-9a-fA-F]{8,16})\\s([rwxp\\-]{4})\\s([0-9a-fA-F]{8,16})\\s(\\d+):(\\d+)\\s(\\d+)\\s(.*)");
    const std::regex expr("([0-9a-fA-F]{8,16})-([0-9a-fA-F]{8,16})\\s([rwxp\\-]{4})");
    std::string line;

    std::vector<std::uintptr_t> sig_result;

    while (std::getline(maps_file, line, '\n'))
    {
        std::smatch result;
        if (std::regex_search(line, result, expr))
        {
            if (result[3].str().find("r", 0, result[3].str().size()))
            {
                // readable
                std::cout << "found readable region" << std::endl;

                std::streampos sstart = Helpers::lexical_cast<unsigned long>("0x" + result[1].str());//std::stoull(result[1].str());
                std::streampos send = Helpers::lexical_cast<unsigned long>("0x" + result[2].str());//std::stoull(result[2].str());

                std::vector<std::uint8_t> data(send - sstart, 0);
                mem_file.seekg(sstart);
                mem_file.read((char *)data.data(), send - sstart);

                std::cout << "sstart " << std::hex << sstart << std::endl;
                std::cout << "send " << std::hex << send << std::endl;
                std::cout << "size of dumped data " << data.size() << std::endl;

                std::uintptr_t pdata = reinterpret_cast<std::uintptr_t>(data.data());

                FindPattern(pdata, data.size(), 0, "55 48 89 E5 BE ?? ?? ?? ?? BF ?? ?? ?? ?? E8 ?? ?? ?? ?? BE ?? ?? ?? ??", sig_result, nullptr, false);

                std::cout << "search done" << std::endl;

                if (sig_result.empty())
                {
                    std::cout << "no sig result" << std::endl;
                    return 0;
                }

                std::cout << "> " << sig_result.front() - pdata + sstart << " " << *(int*)(sig_result.front() - pdata + sstart) << std::endl;
                std::cout << "> " << reinterpret_cast<std::uintptr_t>(&fn) << " " << *(int*)(&fn) << std::endl;

                typedef void (*fn_t)(void);
                fn_t pfn = reinterpret_cast<fn_t>(sig_result.front() - pdata + sstart);

                std::cout << "pfn " << sig_result.front() << std::endl;

                // calling fn
                std::cout << "calling fn : ";
                fn();

                // calling fn from sig
                std::cout << "calling fn from sig : ";
                pfn();
            }
        }
        else
            std::cout << "no regex match" << std::endl;
    }

    return 0;
}