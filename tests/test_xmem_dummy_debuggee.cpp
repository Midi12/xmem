//
// Created by Midi12 on 19/03/16.
//

#include "../xmem/XMem.hpp"
#include <iostream>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

using namespace XMem;

static char s_name[] = "XMem";

typedef struct TEST_t
{
    void *p;
    int m;
    int n;
    void *o;
} TEST, *PTEST;

unsigned long long genhb(void)
{
    return ((rand()) ^ 0xDEADBEEF ^ 0xCAFEBABE) & 0xFF;
}

int main(int argc, char* argv[])
{
    std::cout << s_name << " dummy debuggee started" << std::endl;
    std::cout << "Press Ctrl-C to stop" << std::endl;

    srand((unsigned int)time(NULL));

    TEST test;
    static PTEST ptest = nullptr;
    ptest = &test;

    for (;;)
    {
        sleep(2);
        std::cout << "Heartbeat : " << std::hex << genhb() << std::endl;

        test.p = reinterpret_cast<void *>(genhb() << 24 | genhb() << 16 | genhb() << 8 | genhb());
        test.m = genhb() << 24 | genhb() << 16 || genhb() << 8 | genhb();
        test.o = &test.n;
        test.n = test.m;
    }

    return 0;
}