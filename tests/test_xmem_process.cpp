//
// Created by Midi12 on 25/02/16.
//

#include "../xmem/XMem.hpp"

#include <iostream>
#include <algorithm>

using namespace XMem;

int main(int argc, char* argv[])
{
    std::cout << "cheching for privileges ... " << std::endl;

    if (!Helpers::check_for_elevated_privileges())
    {
        std::cout << "necessary privileges not held, retry with root privileges" << std::endl;
        return 0;
    }

    std::cout << "search for test_xmem_process ..." << std::endl;

    std::vector<Process> processes = Process::EnumerateProcesses();

    auto it = std::find_if(processes.begin(), processes.end(), [](const Process& item)
    {
        return item.GetName().find("test_xmem_process") != std::string::npos;
    });

    if (it != processes.end())
    {
        std::cout << "FOUND " << processes.size() << std::endl;
        std::cout << (*it).GetName() << std::endl;
        std::cout << (*it).GetPath() << std::endl;
        std::cout << (*it).GetPid() << std::endl;
        std::cout << "Process::EnumerateProcesses() test completed successfully !" << std::endl;
    }
    else
    {
        std::cout << "Process::EnumerateProcesses() test failed !" << std::endl;
    }

    processes.clear();
    processes = Process::EnumerateProcesses("test_xmem_process");

    if (processes.size() > 0)
    {
        std::cout << "FOUND " << processes.size() << std::endl;
        std::cout << processes[0].GetName() << std::endl;
        std::cout << processes[0].GetPath() << std::endl;
        std::cout << processes[0].GetPid() << std::endl;
        std::cout << "Process::EnumerateProcesses(const std::string&) test completed successfully !" << std::endl;
    }
    else
    {
        std::cout << "Process::EnumerateProcesses(const std::string&) test failed !" << std::endl;
    }

    return 0;
}