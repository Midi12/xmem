//
// Created by Midi12 on 25/02/16.
//

#include "../xmem/XMem.hpp"
#include <iostream>

using namespace XMem;

typedef struct TEST_t
{
    void *p;
    int m;
    int n;
    void *o;
} TEST, *PTEST;

int main(int argc, char* argv[])
{
    std::cout << "cheching for privileges ... " << std::endl;

    if (!Helpers::check_for_elevated_privileges())
    {
        std::cout << "necessary privileges not held, retry with root privileges" << std::endl;
        return 0;
    }

    std::vector<Process> processes = Process::EnumerateProcesses();

    auto it = std::find_if(processes.begin(), processes.end(), [](const Process& item)
    {
        return item.GetName().find("test_xmem_dummy_debuggee") != std::string::npos;
    });

    if (it != processes.end())
    {
        std::cout << "FOUND " << it->GetPid() << std::endl;

        it->Attach();

        if (!it->IsAttached())
        {
            std::cout << "cannot attach to debuggee" << std::endl;
            return -1;
        }

        // read original values

        // read string
        std::string s = it->Read<std::string>(0x602098);
        std::cout << "read " << s << " expected XMem" << std::endl;

        // read integral
        int x = it->Read<int>(0x602098); //6d654d58
        std::cout << "read " << std::hex << x << " expected 6d654d58" << std::endl;

        // read pointer
        std::uintptr_t pt = it->Read<std::uintptr_t>(0x6021E0);

        // read struct
        TEST t = it->Read<TEST>(pt);

        std::cout << "t.p = " << t.p << std::endl;
        std::cout << "t.m = " << t.m << std::endl;
        std::cout << "t.n = " << t.n << std::endl;
        std::cout << "t.o = " << t.o << std::endl;

        // tamper values
        std::string s2 = "Midi";

        t.p = nullptr;
        t.m = t.m == 1 ? 0 : 2;
        t.n = t.m;
        t.o = nullptr;

        // write string
        it->Write<std::string>(0x602098, s2);

        // write struct
        it->Write<TEST>(pt, t);

        // read tampered values

        // read string
        s = it->Read<std::string>(0x602098);
        std::cout << "read " << s << " expected Midi" << std::endl;

        // read integral
        x = it->Read<int>(0x602098); //6964694d
        std::cout << "read " << std::hex << x << " expected 6964694d" << std::endl;

        // read pointer
        pt = it->Read<std::uintptr_t>(0x6021E0);

        // read struct
        t = it->Read<TEST>(pt);

        std::cout << "t.p = " << t.p << std::endl;
        std::cout << "t.m = " << t.m << std::endl;
        std::cout << "t.n = " << t.n << std::endl;
        std::cout << "t.o = " << t.o << std::endl;
    }
    else
    {
        std::cout << "cannot find debuggee" << std::endl;
    }

    return 0;
}