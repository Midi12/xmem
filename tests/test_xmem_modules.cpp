//
// Created by Midi12 on 25/02/16.
//

#include "../xmem/XMem.hpp"

#include <iostream>
#include <algorithm>

using namespace XMem;

int main(int argc, char* argv[])
{
    std::cout << "cheching for privileges ... " << std::endl;

    if (!Helpers::check_for_elevated_privileges())
    {
        std::cout << "necessary privileges not held, retry with root privileges" << std::endl;
        return 0;
    }

    std::cout << "search for test_xmem_modules ..." << std::endl;

    std::vector<Process> processes = Process::EnumerateProcesses("test_xmem_modules");

    if (processes.size() > 0)
    {
        std::cout << "process found ! enumerating modules for pid " << std::to_string(processes[0].GetPid()) << std::endl;
        for (auto& mod : Module::EnumerateModules(processes[0].GetPid()))
        {
            std::cout << "mod : " << mod.GetName() << std::endl;
            std::cout << "\tpath : " << mod.GetName() << std::endl;
            std::cout << "\tstart : " << mod.GetStartAddress() << std::endl;
            std::cout << "\tend : " << mod.GetEndAddress() << std::endl;
            std::cout << "\tsize : " << mod.GetSize() << std::endl;
        }
    }
    else
    {
        std::cout << "Process::EnumerateProcesses() test failed !" << std::endl;
    }
}