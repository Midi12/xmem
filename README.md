# XMem #

## Purpose ##

XMem is a light cross platform process & memory management library

## Features ##

* Process management
* Modules management
* Process memory reading & writing
* Pattern search on memory region