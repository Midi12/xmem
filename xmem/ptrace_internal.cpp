//
// Created by Midi12 on 25/02/16.
//

#include "ptrace_internal.hpp"

//todo : add std::wstring

namespace ptrace_internal
{
    /*
     * set ptrace last error
     */
    void set_last_error()
    {
        s_ptrace_lasterror = errno;
    }

    /*
     * get ptrace last error
     */
    int get_last_error(void)
    {
        return s_ptrace_lasterror;
    }

    /*
     * attach to process identified by "pid"
     * return true if successful
     */
    bool attach(pid_t pid)
    {
        int err = ptrace(PTRACE_ATTACH, pid, NULL, NULL);

        if (err == -1)
        {
            set_last_error();
            return false;
        }

        return true;
    }

    /*
     * detach from process identified by "pid"
     * return true if successful
     */
    bool detach(pid_t pid)
    {
        int err = ptrace(PTRACE_DETACH, pid, NULL, NULL);

        if (err == -1)
        {
            set_last_error();
            return false;
        }

        return true;
    }

    /*
     * write data to process at specified address
     * return true if successful
     */

    /*
     * specialize for integral types
     */
    /*template <typename T, typename std::enable_if_t<std::is_integral<T>::value>>
    bool write(pid_t pid, std::uintptr_t address, const T& value)
    {
        int ret = ptrace(PTRACE_POKEDATA, pid, address, value);
        if (ret == -1)
        {
            set_last_error();
            return false;
        }

        return true;
    }*/

    /*
     * specialize for floating point types
     */
    /*template <typename T, typename std::enable_if_t<std::is_floating_point<T>::value>>
    bool write(pid_t pid, std::uintptr_t address, const T& value)
    {
        int ret = ptrace(PTRACE_POKEDATA, pid, address, value);
        if (ret == -1)
        {
            set_last_error();
            return false;
        }

        return true;
    }*/

    /*
     * specialize for class/structs
     */
    /*template <typename T, typename std::enable_if_t<std::is_class<T>::value && !std::is_convertible<T, std::string>::value>>
    bool write(pid_t pid, std::uintptr_t address, const T& value)
    {
        const std::size_t size = sizeof(T);
        void *ptr = value;
        unsigned long tmp;

        //refactor in while loop
        for (int i = 0; i < size; i += sizeof(tmp))
        {
            std::memcpy(&tmp, ptr + i, sizeof(tmp));
            int ret = ptrace(PTRACE_POKEDATA, pid, address + i, tmp);

            if (ret == -1)
            {
                set_last_error();
                return false;
            }
        }

        return true;
    }*/

    /*
     * specialize for std::string
     */

    /*template <>
    inline bool write<std::string>(pid_t pid, std::uintptr_t address, const std::string& value)
    {
        const std::size_t size = value.size();
        std::vector<char> data(value.begin(), value.end());
        void *ptr = reinterpret_cast<void *>(data.data());
        unsigned long tmp;

        for (int i = 0; i < size; i += sizeof(tmp))
        {
            std::memcpy(&tmp, ptr + i, sizeof(tmp));
            int ret = ptrace(PTRACE_POKEDATA, pid, address + i, tmp);

            if (ret == -1)
            {
                set_last_error();
                return false;
            }
        }

        return true;
    }*/
    

    /*
     * read data from specified address
     * return true if successful
     */

    /*
     * specialize for integral types
     */
    /*template <typename T, typename std::enable_if_t<std::is_integral<T>::value>>
    bool read(pid_t pid, std::uintptr_t address, T& value)
    {
        int ret = ptrace(PTRACE_PEEKDATA, pid, address, 0);
        if (ret == -1)
        {
            set_last_error();
            if (get_last_error() != 0)
                return false;
        }

        value = static_cast<T>(ret);
        return true;
    }*/

    /*
     * specialize for floating types
     */
    /*template <typename T, typename std::enable_if_t<std::is_floating_point<T>::value>>
    bool read(pid_t pid, std::uintptr_t address, T& value)
    {
        int ret = ptrace(PTRACE_PEEKDATA, pid, address, 0);
        if (ret == -1)
        {
            set_last_error();
            if (get_last_error() != 0)
                return false;
        }

        value = static_cast<T>(ret);
        return true;
    }*/

    /*
     * specialize for class/structs
     */
    /*template <typename T, typename std::enable_if_t<std::is_class<T>::value && !std::is_convertible<T, std::string>::value>>
    bool read(pid_t pid, std::uintptr_t address, T& value)
    {
        const std::size_t size = sizeof(T);
        void *ptr = value;
        unsigned long tmp;

        // refactor in while loop
        for (int i = 0; i < size; i += sizeof(tmp))
        {
            tmp = ptrace(PTRACE_PEEKDATA, pid, address + i, 0);

            if (tmp == -1)
            {
                set_last_error();
                if (get_last_error() != 0)
                    return false;
            }

            // ADD CHECK FOR BUFFER OVERFLOW
            std::memcpy(ptr + i, &tmp, sizeof(tmp));
        }

        return true;
    }*/

    /*
     * specialize for std::string
     */
    
    /*template <>
    inline bool read<std::string>(pid_t pid, std::uintptr_t address, std::string& value)
    {
        char *ptr = new char[4096]; // 4096 should be ok for most strings
        const int size = 4096;
        unsigned long tmp;
        bool eos = false;
        int i = 0;

        while (!eos)
        {
            if (i + sizeof(tmp) > size)
            {
                delete ptr;     //todo reallocation
                return false;
            }

            tmp = ptrace(PTRACE_PEEKDATA, pid, address + i, 0);
            if (tmp == -1)
            {
                set_last_error();
                if (get_last_error() != 0)
                {
                    ptr[i] = '\0';
                    break;
                }
            }

            std::memcpy(ptr + i, &tmp, sizeof(tmp));

            if (std::memchr(&tmp, '\0', sizeof(tmp)))
                eos = true;
            else
                i += sizeof(tmp);
        }

        value = std::string(ptr);
        return false;
    }*/
};