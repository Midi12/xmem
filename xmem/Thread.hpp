//
// Created by Midi12 on 20/03/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_THREAD_HPP
#define XMEM_THREAD_HPP

#include <vector>
#include <string>
#include <cstdint>

#ifdef __linux__
#include <fstream>
#include <regex>
#include <sys/types.h>
#endif

#include "Types.hpp"

namespace XMem
{
    class Thread
    {
	public:
		enum class eThreadState
		{
			Running,
			Sleeping,
			Uninterruptible,
			Zombie,
			Traced
		};
    public:
        Thread();
		~Thread();

		bool IsMainThread(void) const;
		Tid_t GetThreadId(void) const;
		std::string GetThreadName(void) const;
		std::uintptr_t GetStartOfCode(void) const;
		std::uintptr_t GetEndOfCode(void) const;
		std::size_t GetVirtualMemorySize(void) const;
		eThreadState GetThreadState(void) const;
		
		static std::vector<Thread> EnumerateThreads(Pid_t pid);

	private:
        Tid_t mThreadId;
		bool mIsMainThread;
		std::string mThreadName;
		std::uintptr_t mStartOfCode;
		std::uintptr_t mEndOfCode;
		std::size_t mVirtualMemorySize;
		eThreadState mThreadState;
    };
}

#endif //XMEM_THREAD_HPP
