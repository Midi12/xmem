//
// Created by Midi12 on 29/02/16.
//

#include "Helpers.hpp"

namespace XMem
{
    namespace Helpers
    {
        bool is_numeric(const std::string& str)
        {
            return std::all_of(str.begin(), str.end(), [](char c)
            {
                return isdigit(c)/*c >= '0' && c <= '9'*/;
            });
        }

        std::vector<std::string> split(const std::string& str, char delim)
        {
            std::stringstream ss(str);
            std::string token;
            std::vector<std::string> tokens;

            while (std::getline(ss, token, delim))
                tokens.push_back(token);

            return tokens;
        }

#ifdef __linux__
        bool check_for_elevated_privileges(void)
        {
            return getuid() == 0;
        }
#endif
    }
}