//
// Created by Midi12 on 28/05/16.
//

#include "Signature.hpp"

#include <sstream>
#include <algorithm>

namespace XMem
{
    void FindPattern(std::uintptr_t start, std::size_t lenght, int position, const std::string& pattern, std::vector<std::uintptr_t>& results, std::function<std::uintptr_t(std::uintptr_t p)> func)
    {
        if (pattern.empty())
            return;

        //credit : darthton blackbone source
        const std::uint8_t *cstart = (const std::uint8_t *)start;
        const std::uint8_t *cend = cstart + lenght;

        const std::uint8_t wildcard = 0xCC;
        std::vector<std::uint8_t> vpattern;

        const char delimiter = ' ';

        // setup pattern
        std::vector<std::string> tokens;
        std::string token;

        std::stringstream ss(pattern);

        while (std::getline(ss, token, delimiter))
        {
            if (token == "??")
                vpattern.emplace_back(wildcard);
            else
            {
                std::istringstream hex(token);
                std::uint32_t byte;
                hex >> std::hex >> byte;

                vpattern.emplace_back(byte);
            }
        }

        for (;;)
        {
            const std::uint8_t *res = std::search(cstart, cend, vpattern.begin(), vpattern.end(),
                                                  [&wildcard](std::uint8_t v1, std::uint8_t v2)
                                                  {
                                                      return (v1 == v2 || v2 == wildcard);
                                                  });

            if (res >= cend)
                break;

            if (position != 0)
                res = res + position;

            if (func)
            {
                std::uintptr_t newAddr = func(reinterpret_cast<std::uintptr_t>(res));
                results.emplace_back(newAddr);
            }
            else
                results.emplace_back(reinterpret_cast<std::uintptr_t>(res));

            cstart = res + vpattern.size();
        }
    }

    void FindPattern(std::uintptr_t start, std::size_t lenght, int position, const std::string& pattern, std::vector<std::uintptr_t>& results, std::function<std::uintptr_t(std::uintptr_t p)> func, bool relative)
    {
        if (relative)
        {
            FindPattern(start, lenght, position, pattern, results,
                        [&](std::uintptr_t p) -> std::uintptr_t
                        {
                            std::uintptr_t offset = p - start;

                            if (offset + 4 >= start + lenght)
                                throw std::out_of_range("rva");

                            int relAddr = *(int *) &((const std::uint8_t *) start)[offset];
                            return p + 4 + relAddr;
                        });

            if (func)
            {
                std::vector<std::uintptr_t> tmp = results;
                results.clear();

                for (std::uintptr_t& p : tmp)
                    results.emplace_back(func(p));

            }
        }
        else
            FindPattern(start, lenght, position, pattern, results, func);
    }
}