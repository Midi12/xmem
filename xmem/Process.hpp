//
// Created by Midi12 on 25/02/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_PROCESS_HPP
#define XMEM_PROCESS_HPP

#include <string>
#include <vector>

#include "Defines.hpp"

#ifdef WIN32
#include <Windows.h>
#elif __linux__
#ifdef XMEM_PTRACE
#include "ptrace_internal.hpp"
#endif
#ifdef XMEM_PROCVM
#include "process_vm.hpp"
#endif
#endif

#include "Types.hpp"

#include "Module.hpp"
#include "Thread.hpp"

namespace XMem
{
    class Process
    {
    public:
        Process();
        ~Process();

        const std::vector<Module>& GetModules(void);
        const bool GetModule(const std::string& name, Module& module) const;
        const std::vector<Thread>& GetThreads(void);
        const bool GetThread(unsigned long id, Thread& thread) const;
        const std::string& GetName(void) const;
        const std::string& GetPath(void) const;
        bool IsAttached(void) const;
        bool Attach(void);
        bool Detach(void);
        Pid_t GetPid(void) const;
#ifdef WIN32
        HANDLE GetHandle(void) const;
#endif

        template <typename T>
        T Read(std::uintptr_t address)
        {
            if (!IsAttached())
                return T();

            T value;
#ifdef WIN32
#elif __linux__
#ifdef XMEM_PTRACE
            ptrace_internal::read<T>(mProcessId, address, value);
#endif
#ifdef XMEM_PROCVM
            process_vm::read<T>(mProcessId, address, value);
#endif
#endif
            return value;
        }

        template <typename T>
        void Write(std::uintptr_t address, const T& value)
        {
            if (!IsAttached())
                return; //throw ? -> implement it
#ifdef WIN32
#elif __linux__
#ifdef XMEM_PTRACE
            ptrace_internal::write<T>(mProcessId, address, value);
#endif
#ifdef XMEM_PROCVM
            process_vm::write<T>(mProcessId, address, value);
#endif
#endif
        }

        static std::vector<Process> EnumerateProcesses();
        static std::vector<Process> EnumerateProcesses(const std::string& name);
    private:
        std::vector<Module> mModules;
        std::vector<Thread> mThreads;
        std::string mName;
        std::string mPath;
        bool mIsAttached;
        Pid_t mProcessId;
#ifdef WIN32
        HANDLE mProcessRef;
#endif
    };
}

#endif //XMEM_PROCESS_HPP
