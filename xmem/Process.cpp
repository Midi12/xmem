//
// Created by Midi12 on 25/02/16.
//

#include "Process.hpp"
#include "Helpers.hpp"

#include <algorithm>

#ifdef __linux__
#include <dirent.h>
#include <fstream>
#include <iostream>
#endif

namespace XMem
{
    Process::Process()
    {
        mName = "";
        mIsAttached = false;
        mProcessId = -1;
#ifdef WIN32
        mProcessRef = nullptr;
#endif
    }

    Process::~Process()
    {

    }

    const std::vector<Module>& Process::GetModules(void)
    {
        if (mModules.empty() && mProcessId != -1)
            mModules = Module::EnumerateModules(mProcessId);
        return mModules;
    }

    const bool Process::GetModule(const std::string &name, Module& module) const
    {
        std::vector<Module>::const_iterator it;
        it = std::find_if(mModules.begin(), mModules.end(), [name](const Module& mod)
        {
            return mod.GetName() == name;
        });

        if (it != mModules.end())
        {
            module = *it;
            return true;
        }

        return false;
    }

    const std::vector<Thread>& Process::GetThreads(void)
    {
        if (mThreads.empty() && mProcessId != -1)
            mThreads = Thread::EnumerateThreads(mProcessId);
        return mThreads;
    }

    const bool Process::GetThread(unsigned long id, Thread& thread) const
    {
        std::vector<Thread>::const_iterator it;
        it = std::find_if(mThreads.begin(), mThreads.end(), [id](const Thread& th)
        {
            return th.GetThreadId() == id;
        });

        if (it != mThreads.end())
        {
            thread = *it;
            return true;
        }

        return false;
    }

    const std::string& Process::GetName(void) const
    {
        return mName;
    }

    const std::string& Process::GetPath(void) const
    {
        return mPath;
    }

    bool Process::IsAttached(void) const
    {
        return mIsAttached;
    }

    bool Process::Attach()
    {
        if (!IsAttached())
        {
#ifdef WIN32
#elif __linux__
#ifdef XMEM_PTRACE
            return mIsAttached = ptrace_internal::attach(mProcessId);
#endif
#ifdef XMEM_PROCVM
            // temp trick for process_vm_*
            return (mIsAttached = Helpers::check_for_elevated_privileges());
#endif
#else
            return false;
#endif
        }

        return false;
    }

    Pid_t Process::GetPid(void) const
    {
        return mProcessId;
    }

    std::vector<Process> Process::EnumerateProcesses()
    {
        return EnumerateProcesses("");
    }

    std::vector<Process> Process::EnumerateProcesses(const std::string &name)
    {
        std::vector<Process> processes;
#ifdef WIN32
#elif __linux__
        /* walk over /proc/ */

        static std::string proc_dir = "/proc/";
        DIR *dir = nullptr;
        dirent *current = nullptr;

        dir = opendir(proc_dir.c_str());
        if (dir == nullptr)
            return processes;


        while (current = readdir(dir))
        {
            if (current->d_type == DT_DIR)
            {
                if (Helpers::is_numeric(current->d_name))
                {
                    pid_t procpid = std::stoi(current->d_name);
                    std::string exepath = proc_dir + current->d_name + "/exe";

                    std::vector<char> tmp(PATH_MAX);
                    ssize_t readlen = readlink(exepath.c_str(), tmp.data(), PATH_MAX - 1);

                    if (readlen != -1)
                    {
                        tmp[readlen] = '\0';
                        std::string procpath(tmp.data());

                        std::string procname = procpath.substr(procpath.find_last_of("/") + 1, procpath.length() - 1);

                        if (name == procname || name == "")
                        {
                            /*std::cout << "rl = " << std::to_string(readlen) << " p = " << exepath << std::endl;
                            std::cout << procpath << std::endl;
                            std::cout << "pn = " << procname << " n = " << name << std::endl << std::endl;*/

                            Process proc;
                            proc.mName = procname;
                            proc.mPath = procpath;
                            proc.mProcessId = procpid;
                            proc.mIsAttached = false;
                            processes.push_back(proc);
                        }
                    }
                }
            }
        }

        closedir(dir);
#else
        return std::vector<Process>();
#endif
        return processes;
    }

#ifdef WIN32
    HANDLE GetHandle(void) const
    {
        return mProcessRef;
    }
#endif

    bool Process::Detach(void)
    {
        if (IsAttached())
        {
#ifdef WIN32
#elif __linux__
#ifdef XMEM_PTRACE
            return mIsAttached = ptrace_internal::detach(mProcessId);
#endif
#ifdef XMEM_PROCVM
            mIsAttached = false;
            return true;
#endif
#else
            return false;
#endif
        }

        return false;
    }
}