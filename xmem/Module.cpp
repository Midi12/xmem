//
// Created by Midi12 on 25/02/16.
//

#include "Module.hpp"
#include "Helpers.hpp"

#include <iostream>
#include <cstdlib>

#ifdef __linux__
#include <fstream>
#include <regex>
#endif

namespace XMem
{
    Module::Module()
    {
        mName = "";
        mPath = "";
        mSize = 0;
        mStartAddress = 0;
        mEndAddress = 0;
    }

    Module::~Module()
    {

    }

    const std::string &Module::GetName(void) const
    {
        return mName;
    }

    const std::string &Module::GetPath(void) const
    {
        return mPath;
    }

    std::size_t Module::GetSize(void) const
    {
        return mSize;
    }

    std::uintptr_t Module::GetStartAddress(void) const
    {
        return mStartAddress;
    }

    std::uintptr_t Module::GetEndAddress(void) const
    {
        return mEndAddress;
    }

    std::vector<Module> Module::EnumerateModules(Pid_t pid)
    {
        /* read /proc/<pid>/maps */
        std::vector<Module> modules;

        static std::string proc_dir = "/proc/";
        std::string maps_path = proc_dir + std::to_string(pid) + "/maps";

        std::fstream maps_file(maps_path, std::ios::in);

        if (!maps_file.is_open())
            return modules;

        // each line match this pattern : <start address>-<end address> <rights> <offset> <dev> <inode> ... <libpath>
        const std::regex expr("([0-f]{1,16})-([0-f]{1,16}) ([r,w,x,s,p,-]{4}) ([0-f]{8}) ([0-f]{2}:[0-f]{2}) ([0-9]+) +(.*)");

        std::string line = "";
        while(std::getline(maps_file, line))
        {
            //parse line
            std::smatch result;
            if (std::regex_search(line, result, expr))
            {
                Module module;

                module.mStartAddress = Helpers::lexical_cast<unsigned long>("0x" + result[1].str()); //why clion is marking line as error ?
                module.mEndAddress = Helpers::lexical_cast<unsigned long>("0x" + result[2].str()); //why clion is marking line as error ?
                module.mSize = std::abs(module.mEndAddress - module.mStartAddress);
                module.mPath = result[7].str();

                if (!result[7].str().empty())
                    if (result[7].str().find("[") == std::string::npos)
                        module.mName = module.mPath.substr(module.mPath.find_last_of("/") + 1, module.mPath.length() - 1);
                    else
                        module.mName = module.mPath;
                else
                    module.mName = "anonymous";

                modules.push_back(module);
            }
        }

        return modules;
    }
}