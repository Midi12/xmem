//
// Created by Midi12 on 25/02/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_MODULE_HPP
#define XMEM_MODULE_HPP

#include <string>
#include <vector>

#include "Types.hpp"

namespace XMem
{
    class Module
    {
    public:
        Module();
        ~Module();

        const std::string& GetName(void) const;
        const std::string& GetPath(void) const;
        std::size_t GetSize(void) const;
        std::uintptr_t GetStartAddress(void) const;
        std::uintptr_t GetEndAddress(void) const;

        static std::vector<Module> EnumerateModules(Pid_t pid);

    private:
        std::string mName;
        std::string mPath;
        std::size_t mSize;
        std::uintptr_t mStartAddress;
        std::uintptr_t mEndAddress;
    };
}

#endif //XMEM_MODULE_HPP
