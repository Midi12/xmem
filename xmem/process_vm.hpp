//
// Created by Midi12 on 19/03/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_PROCESS_VM_HPP
#define XMEM_PROCESS_VM_HPP

#include <cstdint>
#include <errno.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <string>
#include <vector>

namespace process_vm
{
    /*
     * error enumeration
     */
    enum class process_vm_error
    {
        SUCCESS,
        PARTIAL,
        FAILURE
    };

    /*
     * write data to process at specified address
     * return process_vm_error::SUCCESS if successful
     */
    template <typename T>
    process_vm_error write(pid_t pid, std::uintptr_t address, const T& value)
    {
        iovec local;
        iovec remote;

        local.iov_base = (void *)&value; //reinterpret_cast<void *>(&value);
        local.iov_len = sizeof(T);

        remote.iov_base = reinterpret_cast<void *>(address);
        remote.iov_len = sizeof(T);

        ssize_t bytesWritten = process_vm_writev(pid, &local, 1, &remote, 1, 0);

        if (bytesWritten == -1)
            // check errno
            return process_vm_error::FAILURE;

        if (bytesWritten < sizeof(T))
            // partial write
            return process_vm_error::PARTIAL;

        return process_vm_error::SUCCESS;
    }

    template <>
    inline process_vm_error write<std::string>(pid_t pid, std::uintptr_t address, const std::string& value)
    {
        iovec local;
        iovec remote;

        std::vector<char> data {value.begin(), value.end()};

        local.iov_base = reinterpret_cast<void *>(data.data());
        local.iov_len = value.size();

        remote.iov_base = reinterpret_cast<void *>(address);
        remote.iov_len = value.size();

        ssize_t bytesWritten = process_vm_writev(pid, &local, 1, &remote, 1, 0);


        if (bytesWritten == -1)
            // check errno
            return process_vm_error::FAILURE;

        if (bytesWritten < value.size())
            // partial write
            return process_vm_error::PARTIAL;

        return process_vm_error::SUCCESS;
    }

    /*
     * read data from specified address
     * return process_vm_error::SUCCESS if successful
     */
    template <typename T>
    process_vm_error read(pid_t pid, std::uintptr_t address, T& value)
    {
        iovec local;
        iovec remote;

        local.iov_base = reinterpret_cast<void *>(&value);
        local.iov_len = sizeof(T);

        remote.iov_base = reinterpret_cast<void *>(address);
        remote.iov_len = sizeof(T);

        ssize_t bytesRead = process_vm_readv(pid, &local, 1, &remote, 1, 0);

        if (bytesRead == -1)
            // check errno
            return process_vm_error::FAILURE;

        if (bytesRead < sizeof(T))
            // partial read
            return process_vm_error::PARTIAL;

        return process_vm_error::SUCCESS;
    }

    template<>
    inline process_vm_error read<std::string>(pid_t pid, std::uintptr_t address, std::string& value)
    {
        iovec local;
        iovec remote;

        std::vector<char> data;
        char c;

        local.iov_base = reinterpret_cast<void *>(&c);
        local.iov_len = 1;

        remote.iov_base = reinterpret_cast<void *>(address);
        remote.iov_len = 1;

        ssize_t err;
        ssize_t bytesRead = 0;

        do
        {
            err = process_vm_readv(pid, &local, 1, &remote, 1, 0);

            if (err == -1)
                // check errno
                return process_vm_error::FAILURE;

            bytesRead++;
            data.push_back(c);
            remote.iov_base++;
        } while (c != '\0');

        value.assign(data.begin(), data.end());
		
        return process_vm_error::SUCCESS;
    }
}

#endif //XMEM_PROCESS_VM_HPP
