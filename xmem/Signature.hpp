//
// Created by Midi12 on 28/05/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_SIGNATURE_HPP
#define XMEM_SIGNATURE_HPP

#include <cstdint>
#include <string>
#include <vector>
#include <functional>

namespace XMem
{
    void FindPattern(std::uintptr_t start, std::size_t lenght, int position, const std::string& pattern, std::vector<std::uintptr_t>& results, std::function<std::uintptr_t(std::uintptr_t p)> func);
    void FindPattern(std::uintptr_t start, std::size_t lenght, int position, const std::string& pattern, std::vector<std::uintptr_t>& results, std::function<std::uintptr_t(std::uintptr_t p)> func, bool relative);
}
#endif //XMEM_SIGNATURE_HPP
