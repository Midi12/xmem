//
// Created by Midi12 on 10/05/16.
//

#ifndef PROJECT_TYPES_HPP
#define PROJECT_TYPES_HPP

namespace XMem
{
#ifdef WIN32
    typedef DWORD Pid_t;
    typedef DWORD Tid_t;
#elif __linux__
    typedef pid_t Pid_t;
    typedef pid_t Tid_t;
#endif
}

#endif //PROJECT_TYPES_HPP
