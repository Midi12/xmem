//
// Created by Midi12 on 10/05/16.
//

#ifndef PROJECT_DEFINES_HPP
#define PROJECT_DEFINES_HPP


#define XMEM_PTRACE
//#define XMEM_PROCVM

#if !defined(XMEM_PTRACE) && !defined(XMEM_PROCVM)
    #error "You should define at least XMEM_PTRACE or XMEM_PROCVM"
#endif

#if defined(XMEM_PTRACE) && defined(XMEM_PROCVM)
    #error "You cannot define both XMEM_PTRACE and XMEM_PROCVM"
#endif

#endif //PROJECT_DEFINES_HPP
