//
// Created by Midi12 on 25/02/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_HELPERS_HPP
#define XMEM_HELPERS_HPP

#include <string>
#include <sstream>
#include <algorithm>
#include <vector>

#ifdef __linux__
#include <unistd.h>
#endif

namespace XMem
{
    namespace Helpers
    {
        bool is_numeric(const std::string& str);

        std::vector<std::string> split(const std::string& str, char delim);

        template <typename T>
        inline T lexical_cast(const std::string &in)
        {
            if (in.size() <= 0 || in.empty())
                return 0;

            std::stringstream ss(in);
            T out = 0;

            //if(!(in >> std::hex >> out))
                //throw
            ss >> std::hex >> out;
            return out;
        };

#ifdef __linux__
        bool check_for_elevated_privileges(void);
#endif

    }
}

#endif //XMEM_HELPERS_HPP
