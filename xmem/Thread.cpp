//
// Created by Midi12 on 20/03/16.
//

#include "Thread.hpp"
#include "Helpers.hpp"
#include <dirent.h>

namespace XMem
{
	Thread::Thread()
	{
		mThreadId = 0;
		mIsMainThread = false;
	}

	Thread::~Thread()
	{

	}

	bool Thread::IsMainThread(void) const
	{
		return mIsMainThread;
	}

	Tid_t Thread::GetThreadId(void) const
	{
		return mThreadId;
	}

	std::vector<Thread> Thread::EnumerateThreads(Pid_t pid)
	{
		/* read /proc/<pid>/task/<tid>/stat */
		std::vector<Thread> threads;

		static std::string proc_dir = "/proc/";
		std::string task_path = proc_dir + std::to_string(pid) + "/task/";

		DIR *dir = nullptr;
		dirent *current = nullptr;

		dir = opendir(task_path.c_str());
		if (dir == nullptr)
			return threads;


		while (current = readdir(dir))
		{
			if (current->d_type == DT_DIR)
			{
				if (Helpers::is_numeric(current->d_name))
				{
					std::string statpath = task_path + current->d_name + "/stat";

					std::fstream stat_file(statpath, std::ios::in);
					if (!stat_file.is_open())
						continue;

					const std::regex expr("(\\d+)\\s\\((.*)\\)\\s(\\w)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)\\s(\\-?\\d+)");


					std::string line = "";
					std::getline(stat_file, line);

					std::smatch result;
					if (std::regex_search(line, result, expr))
					{
						Thread thread;

						eThreadState state;
						switch (result[3].str().c_str()[0])
						{
							case 'R':
								state = eThreadState::Running;
								break;
							case 'S':
								state = eThreadState::Sleeping;
								break;
							case 'D':
								state = eThreadState::Uninterruptible;
								break;
							case 'Z':
								state = eThreadState::Zombie;
								break;
							case 'T':
								state = eThreadState::Traced;
								break;
							default:
								state = eThreadState::Sleeping;
						}

						thread.mThreadId = std::stoi(result[1].str());
						thread.mIsMainThread = (bool)(std::stoi(result[1].str()) == pid);
						thread.mThreadName = result[2].str();
						thread.mThreadState = state;
						thread.mStartOfCode = std::stoul(result[26].str());
						thread.mEndOfCode = std::stoul(result[27].str());
						thread.mVirtualMemorySize = std::stoul(result[23].str());
						
						threads.push_back(thread);
					}
				}
			}
		}

		closedir(dir);
		return threads;
	}
}
