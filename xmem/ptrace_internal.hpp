//
// Created by Midi12 on 25/02/16.
//

#ifdef MSVC
#pragma once
#endif

#ifndef XMEM_PTRACE_INTERNAL_HPP
#define XMEM_PTRACE_INTERNAL_HPP

#include <sys/types.h>
#include <sys/ptrace.h>
#include <cstdint>
#include <type_traits>
#include <string>
#include <cstring>
#include <vector>
#include <errno.h>

namespace ptrace_internal
{
    static int s_ptrace_lasterror = 0;

    /*
     * set ptrace last error
     */
    void set_last_error();

    /*
     * get ptrace last error
     */
    int get_last_error(void);

    /*
     * attach to process identified by "pid"
     * return true if successful
     */
     bool attach(pid_t pid);

    /*
     * detach from process identified by "pid"
     * return true if successful
     */
    bool detach(pid_t pid);

    /*
     * write data to process at specified address
     * return true if successful
     */
    template <typename T>
    bool write(pid_t pid, std::uintptr_t address, const T& value)
    {
        const std::size_t size = sizeof(T);
        void *ptr = (void *)&value;
        unsigned long tmp;

        //refactor in while loop
        for (int i = 0; i < size; i += sizeof(tmp))
        {
            std::memcpy(&tmp, ptr + i, sizeof(T) < sizeof(tmp) ? sizeof(T) : sizeof(tmp));
            int ret = ptrace(PTRACE_POKEDATA, pid, address + i, tmp);

            if (ret == -1)
            {
                set_last_error();
                return false;
            }
        }

        return true;
    }

    template <>
    inline bool write<std::string>(pid_t pid, std::uintptr_t address, const std::string& value)
    {
        const std::size_t size = value.size();
        std::vector<char> data(value.begin(), value.end());
        void *ptr = reinterpret_cast<void *>(data.data());
        unsigned long tmp;

        for (int i = 0; i < size; i += sizeof(tmp))
        {
            std::memcpy(&tmp, ptr + i, size < sizeof(tmp) ? size : sizeof(tmp));
            int ret = ptrace(PTRACE_POKEDATA, pid, address + i, tmp);

            if (ret == -1)
            {
                set_last_error();
                return false;
            }
        }

        return true;
    }

    /*
     * read data from specified address
     * return true if successful
     */
    template <typename T>
    bool read(pid_t pid, std::uintptr_t address, T& value)
    {
        const std::size_t size = sizeof(T);
        void *ptr = &value;
        unsigned long tmp;

        // refactor in while loop
        for (int i = 0; i < size; i += sizeof(tmp))
        {
            tmp = ptrace(PTRACE_PEEKDATA, pid, address + i, 0);

            if (tmp == -1)
            {
                set_last_error();
                if (get_last_error() != 0)
                    return false;
            }

            // ADD CHECK FOR BUFFER OVERFLOW
            std::memcpy(ptr + i, &tmp, sizeof(T) < sizeof(tmp) ? sizeof(T) : sizeof(tmp));
        }

        return true;
    }

    template <>
    inline bool read<std::string>(pid_t pid, std::uintptr_t address, std::string& value)
    {
        char *ptr = new char[4096]; // 4096 should be ok for most strings
        const int size = 4096;
        unsigned long tmp;
        bool eos = false;
        int i = 0;

        while (!eos)
        {
            if (i + sizeof(tmp) > size)
            {
                delete ptr;     //todo reallocation
                return false;
            }

            tmp = ptrace(PTRACE_PEEKDATA, pid, address + i, 0);
            if (tmp == -1)
            {
                set_last_error();
                if (get_last_error() != 0)
                {
                    ptr[i] = '\0';
                    break;
                }
            }

            std::memcpy(ptr + i, &tmp, sizeof(tmp));

            if (std::memchr(&tmp, '\0', sizeof(tmp)))
                eos = true;
            else
                i += sizeof(tmp);
        }

        value = std::string(ptr);
        return false;
    }
}

#endif //XMEM_PTRACE_INTERNAL_HPP
